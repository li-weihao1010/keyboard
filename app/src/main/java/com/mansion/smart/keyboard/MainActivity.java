package com.mansion.smart.keyboard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private IEditText edit1;
    private IEditText edit2;
    private IKeyboardView keyboardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit1 = findViewById(R.id.edit1);
        edit2 = findViewById(R.id.edit2);
        keyboardView = findViewById(R.id.keyboardview);

        //默认绑定一个
        keyboardView.setEditText(edit1);
        edit1.setmIKeyboardView(keyboardView);
        edit2.setmIKeyboardView(keyboardView);
    }
}